package com.frede.movieserie;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MovieSerieSelection extends AppCompatActivity {

    private TextView movieSelection;
    private TextView serieSelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_serie_selection);

        movieSelection = (TextView) findViewById(R.id.movieSelection);
        serieSelection = (TextView) findViewById(R.id.serieSelection);
        movieSelection.setClickable(true);
        serieSelection.setClickable(true);

        movieSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ActivityMovieList.class);
                startActivity(intent);
            }
        });

        serieSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SerieList.class);
                startActivity(intent);
            }
        });

    }
}
