package com.frede.movieserie;

import java.util.ArrayList;
import java.util.List;

public class MoviesSingleton {
    private static MoviesSingleton ourInstance;
    private List<Movie> moviesList;

    public static MoviesSingleton getInstance() {

        if (ourInstance == null) {
            ourInstance = new MoviesSingleton();
        }

        return ourInstance;
    }

    private MoviesSingleton() {
        if(this.moviesList == null){
            moviesList = new ArrayList<>();
        }
    }

    public List<Movie> getMoviesList() {
        return moviesList;
    }

    public void setMoviesList(List<Movie> moviesList) {
        this.moviesList = moviesList;
    }
}
