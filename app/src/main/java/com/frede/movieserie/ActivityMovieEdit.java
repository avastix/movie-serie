package com.frede.movieserie;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ActivityMovieEdit extends AppCompatActivity {

    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_edit);

        final TextView textViewNombre = (TextView)findViewById(R.id.textViewNombre);
        final TextView textViewDirector = (TextView)findViewById(R.id.textViewDirector);
        final TextView textViewFecha = (TextView)findViewById(R.id.textViewFecha);
        final TextView textViewGenero = (TextView)findViewById(R.id.textViewGenero);

        //Se cojen los datos enviados de la otra Activity
        Intent intent = getIntent();
        int movieId = getIntent().getIntExtra("com.frede.movieserie.ActivityMovieEdit.movieId", 0);
        movie = MoviesSingleton.getInstance().getMoviesList().get(movieId);

        textViewNombre.setText(movie.getNombre());
        textViewDirector.setText(movie.getDirector());
        textViewFecha.setText(movie.getFecha());
        textViewGenero.setText(movie.getGenero());

        Button update = (Button) findViewById(R.id.save);

        update.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {

                movie.setNombre(textViewNombre.getText().toString());
                movie.setDirector(textViewDirector.getText().toString());
                movie.setFecha(textViewFecha.getText().toString());
                movie.setGenero(textViewGenero.getText().toString());
                finish();
            }
        });

        Button back = (Button) findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {

                Intent intent = new Intent(getApplicationContext(), ActivityMovieList.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
