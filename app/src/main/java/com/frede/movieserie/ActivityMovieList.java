package com.frede.movieserie;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class ActivityMovieList extends AppCompatActivity implements View.OnClickListener {

    private ListView listaPelis;
    private MovieAdapter movieAdapter;
    private FABToolbarLayout morph;
    private final Context context = this;
    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);

        options();

        crearLista();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.fab) {
            morph.show();
        }

        if(v.getId() == R.id.asc) {
            asc();
        }

        if(v.getId() == R.id.desc) {
            desc();
        }

        if(v.getId() == R.id.calendar) {
            dialog();
        }

        if(v.getId() == R.id.genero) {
            genero();
        }

        morph.hide();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /* Cuando vas a la siguiente actividad se pausa la activdad actual, por eso cuando vuelves
        tienes que avisar que ha cambiado el adapter*/
        movieAdapter.notifyDataSetChanged();
    }

    private void options() {

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        morph = (FABToolbarLayout) findViewById(R.id.fabtoolbar);

        View asc, desc, calendar, genero;

        asc = findViewById(R.id.asc);
        desc = findViewById(R.id.desc);
        calendar = findViewById(R.id.calendar);
        genero = findViewById(R.id.genero);

        fab.setOnClickListener(this);
        asc.setOnClickListener(this);
        desc.setOnClickListener(this);
        calendar.setOnClickListener(this);
        genero.setOnClickListener(this);
    }

    private void crearLista() {

        movieAdapter = new MovieAdapter(this, MoviesSingleton.getInstance().getMoviesList());

        listaPelis = (ListView)findViewById(R.id.main_movie_list);

        listaPelis.setAdapter(movieAdapter);

        // En la lista, si se pulsa un película durante un rato se puede borrar
        listaPelis.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {

                final int posicion=position;

                // Creación pop up
                AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityMovieList.this);
                dialog.setTitle("Importante");
                dialog.setMessage("¿ Eliminar película ?");
                dialog.setCancelable(false);

                dialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        MoviesSingleton.getInstance().getMoviesList().remove(posicion);
                        movieAdapter.notifyDataSetChanged();
                    }
                });
                dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                    }
                });

                dialog.show();

                return true;
            }
        });

        // En la lista, si se pulsa un película se va a otra actividad donde lo puedes editar
        listaPelis.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getApplicationContext(), ActivityMovieEdit.class);
                intent.putExtra("com.frede.movieserie.ActivityMovieEdit.movieId", position);
                startActivity(intent);
            }
        });
    }

    private void asc() {
        // Ordenación ascendente
        List<Movie> movies = MoviesSingleton.getInstance().getMoviesList();
        Collections.sort(movies);
        movieAdapter.notifyDataSetChanged();
    }

    private void desc() {
        // Ordenación descendente
        List<Movie> movies = MoviesSingleton.getInstance().getMoviesList();
        Collections.sort(movies);
        Collections.reverse(movies);
        movieAdapter.notifyDataSetChanged();
    }

    private void genero() {
        // Ordenación descendente
        List<Movie> movies = MoviesSingleton.getInstance().getMoviesList();
        Collections.sort(movies);
        movieAdapter.notifyDataSetChanged();
    }

    public void dialog() {

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.layout_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText inputDate = (EditText) promptsView
                .findViewById(R.id.yearDate);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                //String yearDate = (inputDate.getText().toString());
                                //movie = MoviesSingleton.getInstance().getMoviesList().get(0);
                                //movie.getFecha().equals(yearDate);
                                //movieAdapter.notifyDataSetChanged();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }
}
