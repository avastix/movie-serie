package com.frede.movieserie;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

public class LanguageSelection extends AppCompatActivity {

    private TextView title;
    private TextView options;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_selection);

        title = (TextView) findViewById(R.id.title);
        options = (TextView) findViewById(R.id.options);
        ImageView flagSpanish = (ImageView) findViewById(R.id.flagSpanish);
        ImageView flagEnglish = (ImageView) findViewById(R.id.flagEnglish);
        flagSpanish.setClickable(true);
        flagEnglish.setClickable(true);

        Locale locale = new Locale("en");
        Locale.setDefault(locale);

        Movie movie = new Movie();
        movie.setNombre("Nombre 1");
        movie.setDirector("Director 1");
        movie.setFecha("2016");
        movie.setGenero("Genero 1");

        MoviesSingleton.getInstance().getMoviesList().add(movie);

        movie = new Movie();
        movie.setNombre("Nombre 2");
        movie.setDirector("Director 2");
        movie.setFecha("2017");
        movie.setGenero("Genero 2");

        MoviesSingleton.getInstance().getMoviesList().add(movie);

        flagSpanish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Locale locale = new Locale("es");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                LanguageSelection.this.getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Intent intent = new Intent(getApplicationContext(), MovieSerieSelection.class);
                startActivity(intent);
                finish();
            }
        });

        flagEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Locale locale = new Locale("en");
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                Intent intent = new Intent(getApplicationContext(), MovieSerieSelection.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
