package com.frede.movieserie;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActivityMovieAdd extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_add);

        final TextView textViewNombre = (TextView)findViewById(R.id.textViewNombre);
        final TextView textViewDirector = (TextView)findViewById(R.id.textViewDirector);
        final TextView textViewFecha = (TextView)findViewById(R.id.textViewFecha);
        final TextView textViewGenero = (TextView)findViewById(R.id.textViewGenero);

        Button add = (Button) findViewById(R.id.add);

        add.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {

                Movie movie = new Movie();

                movie.setNombre(textViewNombre.getText().toString());
                movie.setDirector(textViewDirector.getText().toString());
                movie.setFecha(textViewFecha.getText().toString());
                movie.setGenero(textViewGenero.getText().toString());

                MoviesSingleton.getInstance().getMoviesList().add(movie);

                finish();
            }
        });

        Button back = (Button) findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {

                Intent intent = new Intent(getApplicationContext(), ActivityMovieList.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
