package com.frede.movieserie;

public class Movie implements Comparable<Movie>{

    private int id;
    private String nombre;
    private String director;
    private String fecha;
    private String genero;

    public Movie() {

    }

    public Movie(int id, String nombre, String director, String fecha, String genero) {
        this.id = id;
        this.nombre = nombre;
        this.director = director;
        this.fecha = fecha;
        this.genero = genero;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public int compareTo(Movie other) {

        String movie1 = this.nombre.toLowerCase();
        String movie2 = other.getNombre().toLowerCase();

        return movie1.compareTo(movie2);
    }
}


