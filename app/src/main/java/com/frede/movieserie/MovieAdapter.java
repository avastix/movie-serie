package com.frede.movieserie;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MovieAdapter extends BaseAdapter {

    private Context context;
    private List<Movie> movieList;
    private ArrayList<Movie> arraylist;

    public MovieAdapter(Context context, List<Movie> movieList) {
        this.context = context;
        this.movieList = movieList;
        this.arraylist = new ArrayList<Movie>();
        this.arraylist.addAll(movieList);
    }

    @Override
    public int getCount() {
        return movieList.size();
    }

    @Override
    public Object getItem(int position) {
        return movieList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return movieList.get(position).getId();
    }

    // Generamos la lista de movies
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            Movie movie = movieList.get(position);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_movie, null);
        }

        // Referencias UI
        TextView textViewNombre = (TextView) convertView.findViewById(R.id.textViewNombre);
        TextView textViewDirector = (TextView) convertView.findViewById(R.id.textViewDirector);
        TextView textViewFecha = (TextView) convertView.findViewById(R.id.textViewFecha);
        TextView textViewGenero = (TextView) convertView.findViewById(R.id.textViewGenero);

        // Posición en la listView
        Movie movie = movieList.get(position);

        // Setup
        textViewNombre.setText(movie.getNombre());
        textViewFecha.setText(movie.getFecha());
        textViewDirector.setText(movie.getDirector());
        textViewGenero.setText(movie.getGenero());

        return convertView;
    }
}