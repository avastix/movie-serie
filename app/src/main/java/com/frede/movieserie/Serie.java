package com.frede.movieserie;

public class Serie {

    public String nombre;
    public String temporada;
    public Integer fecha;
    public String genero;

    public Serie() {

    }

    public Serie(String nombre, String temporada, Integer fecha, String genero) {
        this.nombre = nombre;
        this.temporada = temporada;
        this.fecha = fecha;
        this.genero = genero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTemporada() {
        return temporada;
    }

    public void setTemporada(String temporada) {
        this.temporada = temporada;
    }

    public Integer getFecha() {
        return fecha;
    }

    public void setFecha(Integer fecha) {
        this.fecha = fecha;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
